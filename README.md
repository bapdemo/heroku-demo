# bap heroku demo

Supports web endpoints:

  - GET / - this README.md
  - PUT /value - persist a string (request body)
  - GET /value - get the current string (response body)

## Usage

Press F12 to get a JavaScript console
There you can execute the following snippets:

### to set the value

```
fetch("value", { method: "PUT", body: "choose a value" })
  .then(r => console.log(`status: ${r.status}`))
```

### to get the current value

```
fetch("value")
  .then(r => {
    if(r.status === 200) {
      r.text().then(console.log)
    } else {
      console.error("not found")
    }
  })
```

## Exercise hints

You probably just need to configure `src/main/resources/application.properties`