package at.spengergasse.bap.heroku.db;

import at.spengergasse.bap.heroku.db.model.Demo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DemoRepository extends JpaRepository<Demo, Long> {

}
