package at.spengergasse.bap.heroku.web;

import at.spengergasse.bap.heroku.db.DemoRepository;
import at.spengergasse.bap.heroku.db.model.Demo;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class DemoController {
  @Autowired DemoRepository repository;

  @GetMapping
  ResponseEntity<String> hello() {
    var body =
        ""
            + "# bap heroku demo\n"
            + "\n"
            + "Supports web endpoints:\n"
            + "\n"
            + "  - GET / - this README.md\n"
            + "  - PUT /value - persist a string (request body)\n"
            + "  - GET /value - get the current string (response body)\n\n"
            + "## Usage\n\n"
            + "Press F12 to get a JavaScript console\nThere you can execute the following snippets:\n"
            + "\n### to set the value\n"
            + "\n```\n"
            + "fetch(\"value\", { method: \"PUT\", body: \"choose a value\" })\n"
            + "  .then(r => console.log(`status: ${r.status}`))\n"
            + "```\n"
            + "\n### to get the current value\n"
            + "\n```\n"
            + "fetch(\"value\")\n"
            + "  .then(r => {\n"
            + "    if(r.status === 200) {\n"
            + "      r.text().then(console.log)\n"
            + "    } else {\n"
            + "      console.error(\"not found\")\n"
            + "    }\n"
            + "  })\n"
            + "```\n\n"
            + "## Exercise hints\n\n"
            + "You probably just need to configure `src/main/resources/application.properties`\n";

    return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(body);
  }

  @GetMapping("value")
  ResponseEntity<String> value() {
    return repository
        .findById(1L)
        .map(Demo::getDemo)
        .map(v -> ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(v))
        .orElse(ResponseEntity.notFound().build());
  }

  @PutMapping("value")
  @Transactional
  String update(@RequestBody String text) {
    var d = new Demo(1L, text);
    repository.saveAndFlush(d);
    return d.getDemo();
  }
}
